# CoAct Buenos Aires- 2023 y 2024

"CoAct - Ciencia ciudadana para la Justicia Ambiental" es un proyecto impulsado por el Centro de Investigaciones para la transformación (CENIT) de la Universidad Nacional de San Martín (UNSAM) y la Fundación Ambiente y Recursos Naturales (FARN). Busca promover el uso de herramientas de ciencia ciudadana para generar conocimiento y acciones transformadoras de justicia ambiental en el territorio de la cuenca Matanza-Riachuelo (CMR). CoAct relanzó en 2022 la plataforma digital ¿Qué Pasa Riachuelo? (QPR), con financiamiento del programa Horizonte 2020. Durante tres años se co-diseñó QPR junto a habitantes, trabajadores y asociaciones vinculadas a la situación socio-ambiental, que participaron en la definición de los temas y funcionalidades de la plataforma. QPR se enfoca en tres temáticas: el monitoreo de la calidad del agua, los procesos de protección de las áreas naturales y los procesos de relocalización o reurbanización. Permite compartir experiencias de las comunidades acerca de los problemas socio-ambientales; sistematizar información pública y visibilizar nueva información generada por la ciudadanía; y potencialmente ser un canal de interacción entre iniciativas que accionan en la CMR.

Este proyecto busca dar continuidad al trabajo de CoAct, mediante actividades vinculadas a la educación ambiental, de generación de capacidades digitales, y de producción de datos ciudadanos. El objetivo general es promover acciones de transformación hacia la justicia ambiental en la CMR mediante un enfoque de ciencia ciudadana social (CCS). Esto tiene tres dimensiones: académica, de intervención y de divulgación. Las principales actividades del proyecto se desarrollarán en ciclos de talleres orientados a la educación ambiental en dos de las temáticas (calidad del agua y protección de áreas naturales).

En la dimensión académica se estudiarán las transformaciones que generan las prácticas del enfoque de CCS. Produciremos evidencia aplicando instrumentos de co-evaluación a lo largo de los talleres para su análisis y discusión con los co-investigadores. Principalmente se indagará sobre los efectos que tienen distintos tipos de participación en dos aspectos: cambios actitudinales y de aprendizajes.

La dimensión de intervención implica llevar adelante actividades de CCS, dando continuidad a CoAct y al uso de su plataforma QPR en el territorio de la CMR en coordinación con co-investigadores de dos instituciones comunitarias: la Biblioteca Popular Sarmiento de Valentín Alsina y la Biblioteca Popular Virrey del Pino e investigadores que forman parte de la coalición de conocimiento. Siguiendo la propuesta del ciclo de investigación de CoAct (Scheller et. al., 2020), las actividades de esta dimensión se inscriben en las fases de realización de una investigación con la producción de datos ciudadanos, y la de su interpretación con el propósito de transformar los resultados en acciones colectivas. De manera transversal se implementarán consideraciones éticas y de co-evaluación, con el involucramiento directo de los miembros de las bibliotecas en la planificación de las actividades a realizar con otros participantes. Se capacitará a miembros de la comunidad local y docentes en actividades de educación ambiental, se realizarán salidas para producir datos, y se coordinarán para su interpretación junto a otros investigadores del SNCTI miembros de
la coalición de conocimiento de CoAct sesiones colaborativas.

Con la dimensión de divulgación se dará continuidad al vínculo con grupos y organizaciones territoriales para la difusión de QPR en actividades de capacitación en su uso y de generación de datos.

El reporte final detallará las actividades con las bibliotecas, el desarrollo de la implementación, los datasets generados, y los hallazgos resultantes. Se reportarán los mapeos de acciones colectivas co-definidas, y aportes e inquietudes que se identifiquen en las sesiones de discusión del marco de gobernanza de QPR. Se incluirá información cuantitativa sobre las actividades realizadas, los perfiles demográficos de las personas involucradas, y los tipos de participación.

## Objetivos ##
El objetivo general es promover acciones de transformación hacia la justicia ambiental mediante un enfoque de ciencia ciudadana en la CMR. Este objetivo tiene a su vez tres dimensiones que reflejan las principales motivaciones de las distintas organizaciones involucradas en CoAct, y que expresamos en tres objetivos específicos.

### Objetivos específicos ###
- OE1. Dimensión académica: explorar los efectos que tiene la participación en iniciativas de ciencia ciudadana sobre las actitudes de los participantes (en relación a la ciencia, el ambiente y las comunidades en las que se insertan) y de aprendizajes (de conocimiento científico y de ciencia ciudadana, del ambiente y digitales asociados al uso de herramientas y datos abiertos). 

- OE2. Dimensión de intervención: utilizar la herramienta QPR para generar, interpretar y explotar datos en temas claves para la justicia ambiental en los que CoAct viene trabajando (calidad del agua y conservación de aras naturales). 

- OE3. Dimensión de divulgación: comunicar el potencial de la ciencia ciudadana para acciones de transformación hacia la justicia ambiental en el territorio de la CMR a través de distintas actividades y recursos que presenten e integren dinámicas de capacitación, generación e interpretación de datos ciudadanos.

## ¿Cómo se utiliza GitLab? ##


### Issues ###

Los issues son creados para reportar la realización de talleres y actividades, así como sus tareas/productos asociados. Los issues están agrupados dentro del milestone correspondiente. Eventualmente, cada issue puede ser asignado a un o una integrante del equipo.

#### Milestones ####

Los milestones son creados para designar las actividades del proyecto. Estos agrupan los issues creados dentro de cada repositorio.

### Labels ### 

Las labels permiten clasificar los issues e identificarlos. En este repositorio, contamos con tres categorías de labels, diferenciados según un código de colores

## ¿Qué y dónde encontrar dentro de este repositorio? ##



| Ítems  | Función/lugar de GitLab  | Descripción | Particularización  |
| :------- | :------: | :------: | -----: |
| Objetivo general   | README       | Escrito en la portada del proyecto   | Objetivo general para los años 2023 y 2024   |
| Objetivos específicos   | MILESTONES      | En su descripción, se los desarrolla. Se los nombra por su código. Son 4: tres objetivos y otro que corresponde a tareas generales | - OE1 - OE2 - OE3 - Generales |
| Actividades   | ISSUES     | Para el año 2023, tenemos planteados ocho talleres de implementación. 4 con la Biblioteca de Val. Alsina y 4 con la de V. del Pino. También se incluyen otras actividades | Se generarán nuevos reportes a medida que las actividades tengan lugar y se describirá lo sucedido |
| Fases del ciclo de investigación de la CC    | LABELS (Naranjas)     | En su descripción, se los desarrolla. Son 4 | Codiseño: co-diseño de la investigación - Investigación: realización de la investigación - Interpretación: interpretación de los datos - Transformación: transformación de los resultados en acción |
| Fases del financiamiento del Prog Nac de CC   | LABELS (Celestes)     | Según el tipo de tareas implicadas, y acorde al cronograma trazado. Son 4  | Preparación - Implementación - Análisis - Reporte |
| Actores institucionales recurrentes   | LABELS (Rojos)     | Actores que forman parte de la colición de conocimiento. Son 8 | CENIT - FARN - Reserva Natural de Ciudad Evita - Biblioteca Popular Sarmiento de Valentín Alsina - Biblioteca Popular Virrey del Pino - Junta de Estudios históricos y culturales de Villa Lugano y Villa Riachuelo - Comunidad por el Pulmón Verde Esperanza - Instituto Superior De Formación Docente Nº1 “Abuelas de Plaza de Mayo” |
| Actores, personas o instituciones particulares   | En cada posteo según el caso     | Anonimizados  | Según el código interno que el equipo de CENIT maneja |

