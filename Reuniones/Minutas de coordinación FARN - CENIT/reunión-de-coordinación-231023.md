_Participantes_: Participantes: Valeria Arza (CENIT), Leticia Castro (CENIT), Guillermina Actis (CENIT), Catalina Fixman (CENIT), Marcelo Larrocca Ruiz (FARN) 

# AGENDA #
1. Talleres de implementación. VdP: ¿nueva serie de talleres?. Alsina: mapeo de sumideros. 
2. Representante de museo en cuenca baja (contacto de ML): propuesta de actividad conjunta
3. Desarrollo-datasets: carga de sets de datos previos (acelerar para backup de la info en web) 
4. Reunión tenida con representante de U2. Posibilidades de articulación alrededor de Educación Ambiental.
5. Novedades en Causa Mendoza, ¿Hay desde FARN acciones legales plasmables en reportes para la plataforma? 
6. Tener actualizada web de CoAct: notas, actualizaciones, secciones, etc.
7. 12/10- jornada en Profesorado del Gran BsAs
8. 18/10- taller de reescritura guía Ed Amb, Versión 2.
9. Posible nexo con organismo del gobierno municipal.
10. CF: Encuentro sobre Ed Ambiental. 
11. CF: Estrategia propia de Ed Ambiental. 
12. Posibilidades de financiamiento.

# RESOLUCIONES #
1. Complicaciones contextuales varias para realización de los talleres planificados. 
2. Aún a la espera de respuesta, luego que hagan evaluación interna de nuestra idea.
3. Desarrollador tendría novedades de tema datasets, pero aún no respondió. Para carga de datasets, se está siguiendo el video tutorial de quien en su momento nos asistió. Otros pendientes sencillos fueron resueltos. Desarrollador firmó y cobró contrato nuevo.
4. Mandamos email para hacer reunión, se propusieron fechas, pero no respondieron.  
5. Mañana reunión de FARN. Necesidad de "traducir" reportes a un lenguaje más llano para subir novedades a la plataforma. Algunas son insumos y otras se convierten en movimientos concretos en el expediente. ML: apenas tenga lo subible, lo comparte para ver manera en que se ha subido, y necesidad de que esté asociado a la publicación en redes.
6. FARN está actualizando su site, con la posibilidad de renovarlo completamente. Se subieron algunas notas, y restan menos por subir. 
7. Fueron JF y CF: Presentamos CC y CoAct. Secuencia a partir de los temas calidad de agua y residuos. Buena experiencia para difusión del proyecto.
8. Hubo poca asistencia. Surge necesidad de parte de docentes tener armado el proyecto para presentar en la escuela a la hora de armar actividades relacionadas con QPR
9. En el marco de su trabajo con villas y asentamientos, harán salida a Reserva de Lugano. Existe posibilidad de hacer talleres de implementación con ellxs. Invitaron a planificación para diagramar dentro de su esquema-calendario. Necesidad de bajar a lenguaje más simple, de divulgación. 
10. No mucha posibilidad de participación de nuestra parte
11. Necesidad de trazar dirección entre el equipo de CENIT
12. A partir de reuniones con reparticiones ministeriales nacionales, se nos pidió un presupuesto para ver factibilidad de búsqueda.

# PENDIENTES #
1. CF hace una evaluación de lo hecho en pos de armar algo para año entrante, y a partir de allí establecer alianzas. Trabajo asincrónico, armado de proyectos y compartirlo con docentes para que puedan presentarlos en planificación anual en febrero. 
2. ML pregunta si lo pudieron conversar internamente
3. Subir los tutoriales en la landing. Por lo demás ya mencionado, ML volverá a consultar.
4. GA manda propuestas para reuniones.
5. ML lo comparte con un breve resumen que desde CENIT se lee/edita para ponerlo en lenguaje más comprensible.
6. Actualizar las pendientes 
7. Sistematizar resultados (CF y JF). Una vez tengamos el reporte, GA compartirlo y pedir reunión para pensar agenda para el año entrante.
8. Armar un template que les sirva a las escuelas para su planificación. CF le va a pedir algún modelo a Biblio de VdP.  
9. CF: Escribirá para ver si tienen fechas definidas.
10. A expectativa de lo que le trasmitan a CF 
11. Se reúne CENIT para tratar el tema, probablemente el 1/11 
12. CENIT y FARN piensan el monto de presupuesto
