_Participantes:_ Valeria Arza (CENIT), Leticia Castro (CENIT), Guillermina Actis (CENIT), Jeremías Fabiano (CENIT), Marcelo Larrocca Ruiz (FARN).

# AGENDA #
1. Talleres de implementación  
2. Charlas con museo de cuenca baja: propuesta de actividad conjunta
3. Desarrollo- datasets: carga de sets de datos previos (acelerar para backup info en repositorios virtuales) 
4. Reunión con área de Educación Ambiental de U2 
5. Novedades en Causa Mendoza. 
6. 12/10: jornada en Profesorado de cuenca media. "X Jornada de Investigación y V de divulgación Académica" 
7. Acciones legales plasmables desde FARN en reportes para la plataforma 
8. CF: posible nexo con organismo municipal
9. CF: Encuentro Nacional de Educación Ambiental 
10. Estrategia de Educación Ambiental

# RESOLUCIONES #
1. Narración sobre desarrollo del anterior taller en Val Alsina. En noviembre se planea en conjunto un taller de mapeo de sumideros. Taller de reescritura con Virrey del Pino a realizarse el 18/10 en formato virtual y su logística. A ppios del mes entrante se haría otra serie de talleres sobre AANN en VdP.
2. Se envía la propuesta de charla-actividad conjunta
3. Se envió mensaje con instrucciones al desarrollador
4. Aún no nos han respodido el email con el acercamiento realizado
5. Resolución sobre navegabilidad próxima a emitirse. 
6. Nuevo taller de difusión en profesorado de cuenca media: posibilidad de mapear actores para conocer vinculaciones.
7. CENIT tendrá reunión informal con funcionario para conocer fuentes de información valiosa. Necesidad de tener documentación de reportes para QPR, así como de actividades realizadas durante el año en el proyecto.
8. NA sigue trabajando en ese organismo municipal
9. Encuentro nac del 2 y 3 de noviembre, con feria en la que CoAct podría participar.
10. Algunos insumos reunidos para la redacción.

# PENDIENTES #
1. Alsina: se hará salida para mapear sumideros  
GM confirma si podrá estar en taller virtual del 18/10  
VdP: nueva serie de talleres a fin de mes y ppios del siguiente: Taller 1: 26/10, 13:30 / Taller 2: 15/11, a confirmar
2. Consultar a nuestro nexo sobre actividades regulares para sumar convocatoria a la propuesta planteada
3. Desarrollador debe subir datasets para probar la función, y a partir de ahí se le encargarán más
4. GA: en caso de que a mediados de prox semana no contesten, se le enviará mail al área
5. ML y GM charlan el tema para pensar respaldo científico que densificaría una toma de posición en torno a la navegabilidad
6. GM avisa si podrá estar presente
7. ML: carga a QPR acciones de FARN, con ayuda de CENIT 
8. CF dinamiza el relacionamiento
9. CF brindará más novedades al respecto a medida que las haya
10. CF: arma la estructura.
