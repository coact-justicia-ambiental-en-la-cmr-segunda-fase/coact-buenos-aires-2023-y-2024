_Participantes:_ Valeria Arza (CENIT), Leticia Castro (CENIT), Guillermina Actis (CENIT), Catalina Fixman (CENIT), Jeremías Fabiano (CENIT), Marcelo Larrocca Ruiz (FARN), Giselle Munno Dithurbide (FARN)

# AGENDA #
1. Talleres de implementación: reunión del 18/12 
2. Búsqueda de financiamientos posibles 
3. Situación de Pulmón Verde 
4. Desarrollo de QPR. Carga de datasets previos. Estado general de los desarrollos 
5. Novedades en Causa Mendoza.  
6. Reunión de comunicación de FARN el 29/11. Actualización web de CoAct: notas, actualizaciones, secciones, etc.
7. Estrategia de Educación Ambiental.
8. Próxima reu de coordinación 

# RESOLUCIONES #
1. Lunes 18/12 de 14 a 16hs en el Volta. Necesidad de tener presentes los compromisos de cada organización, los roles bien definidos a ocupar de ahora en más y en claro el texto del proyecto para guiar la discusión y optimizar el tiempo.
2. Nos hemos inscripto en una iniciativa de datos ambientales abiertos, que tienen una línea para la apertura de datos.
3. No queda claro qué quiere hacer el municipio allí. No tenemos registro de un estudio de impacto ambiental previo a esta avanzada. Sería bueno saber al respecto, y poder participar de las mesas o instancias que pueda haber. También, se podrían hacer unas jornadas de registro en el espacio y registrar en novedades todo lo que estuvo pasando. 
4. Se solucionó la carga de pdfs. Clusterización se comenzó a implementar. Ya se pueden subir novedades del cuerpo colegiado. Resta realizar protocolo para subida de nuevos sets de datos.  
5. - (charlado en punto anterior) 
6. Se lanzó idea de un boceto de cómo podría ser el nuevo sitio web. 
7. FARN comenzó a revisar el documento elaborado
8. Necesidad de tener la última reunión del año, sobre todo luego de la reunión presencial del 18/12

# PENDIENTES #
1. JF: escribe a Biblio Sarmiento. FARN: continúa seguimiento de quienes le falten chequear. 
2. A la espera de resultados, que no creemos sea antes del 18/12.
3. FARN: asesora y asiste legalmente a Pulmón Verde. FARN: habla con área de biodiversidad sobre posibilidad de activar instancias de carga de reportes sobre el Pulmón 
4. Que se puedan descargar bien todos los datasets (ordenados los campos). JF: fijarse si está documentado y de qué manera para entendimiento ajeno a CENIT-FARN
5. Hay algunos reportes a cargarse en la plataforma 
6. FARN: revisa la comunicación.
7. Entre FARN-CENIT continúan viendo documento de estrategia 
8. Próxima reunión de coordinación FARN-CENIT jueves 21/12 a las 14hs
