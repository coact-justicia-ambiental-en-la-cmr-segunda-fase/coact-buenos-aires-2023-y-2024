_Participantes:_ Valeria Arza (CENIT), Leticia Castro (CENIT), Guillermina Actis (CENIT), Catalina Fixman (CENIT), Jeremías Fabiano (CENIT), Marcelo Larrocca Ruiz (FARN)

# AGENDA #
1. Talleres de implementación. Alsina: mapeo de sumideros. Evaluar lo realizado. Nexos con organizaciones.  
2. Representante de museo en cuenca baja (contacto de ML): propuesta de actividad conjunta 
3. Desarrollo-datasets. Carga de sets de datos previos. Estado general de los desarrollos 
4. Reunión con Educación Ambiental de U2.  
5. Novedades en Causa Mendoza. Reunión FARN tenida sobre Riachuelo. Traducción de documentos, informes y demás escritos para cargar reportes a la plataforma
6. Tener actualizada web de CoAct: notas, actualizaciones, secciones, etc.
7. Taller de reescritura guía Ed Amb, Versión 2. 231018 Taller final de revisión secuencias didácticas. 
8. Nexo con organismo de vivienda del gobierno municipal.
9. Estrategia de Educación Ambiental.  
10. Nueva audiencia pública de U2 

# RESOLUCIONES #
1. Reunión anual será clave para planificar lo venidero al respecto. Necesidad de definir los gastos lo antes posible. 
2. Abiertos a cualquiera de las dos propuestas: tanto charla general o como la focalizada en ed ambiental
3. Carga de pdf a la plataforma está en proceso. Correcciones a realizarse sobre preguntas en formularios de QPR. Subidos datasets pendientes. 
4. Desde repartición de U2 tuvieron un problema y no se pudo realizar la reunión.
5. FARN comenzó planilla con subibles pendientes: necesidad de simplificar el lenguaje. Reunión de planeamiento del año que viene entre diferentes organizaciones parte de CoAct: invitación y logística.
6. No se ha definido qué pasará con sitio web. 
7. Necesidad de obtener todo el material resultante y sistematizarlo. 
8. Finalmente no se hará la salida, por lo que nos darán contactos para tener vínculo más directo. 
9. La idea es que si armamos un buen documento que podamos presentar a actores nuevos o a la hora de pedir un financiamiento. Hay una parte teórica y antecedentes. Talleres, con educación formal y no formal. Hay un jamboard con un FODA para barajar ideas. 
10. Necesidad de tener más información sobre temario y carácter de la participación.

# PENDIENTES #
1. A la espera de reunión lanzamiento proy del 16/11 en el campus UNSAM
2. Vamos por la opción de charla general
3. JF comunica al canal de desarrollo fallas actuales y desarrollos pendientes. Pendiente protocolos de subida
4. GA escribe nuevamente
5. ML pasa a archivo de Word para hacer sugerencias e ir puliendo. CENIT trabaja a partir de allí. Jueves 16 de noviembre reunión entre las 14 y las 16hs en el campus.
6. 29/11 se hará reunión de comunicación de FARN. 
7. Sistematización pendiente para presentar a docentes material procesado.
8. A la espera de que nos brinden el contacto con la escuela en cuestión 
9. FARN revisa la estrategia. CF y CENIT continúan nutriendo el documento.
10. ML informa qué saben o harán desde FARN al respecto. 
