_Participantes:_ Valeria Arza (CENIT), Leticia Castro (CENIT), Guillermina Actis (CENIT), Catalina Fixman (CENIT), Jeremías Fabiano (CENIT), Marcelo Larrocca Ruiz (FARN), Giselle Munno Dithurbide (FARN)

# AGENDA #
1. Talleres de implementación. Alsina: cosensores y mapeo de sumideros. Evaluar lo realizado.
2. Actores para pensar en la reunión del 18/12: representante de museo en cuenca baja, propuesta de actividad conjunta 
3. Desarrollo- datasets. Carga de sets de datos previos. Estado general de los desarrollos. 
4. Educación Ambiental de U2. 
5. Novedades en Causa Mendoza. Traducción de documentos, informes y demás escritos para cargar reportes a la plataforma.
6. Profesorado en Gran BsAs: contacto a nuestro nexo para comenzar a pensar agenda del año venidero. 
7. Nueva audiencia pública U2 


# RESOLUCIONES #
1. Toda planificación será para la reunión de fin de año del 18/12 en Edificio Volta. Resta confirmación de algunos invitados. Exploración de financiamientos y utilidades de la plataforma en diferentes ámbitos.
2. De parte del lugar, creen que podríamos hacer algo antes del 20 de diciembre.
3. Todo bastante encaminado, por ende necesidad de pensar las urgencias que haya al respecto.
4. Se vislumbran complicaciones para realizar grandes actividades. 
5. Ante solución técnica en subida de pdfs a la plataforma, posibilidad de subir reportes legales desde FARN. 
6. Elaborado documento para mandarle a instituciones con las que queramos trabajar: se fueron marcando correcciones sobre el archivo y se hizo un glosario. También se hizo un modelo de realización de talleres.
7. Se puede hacer la mención de que se están identificando áreas naturales y del rol de la Ciencia Ciudadana. Necesidad de dejar plasmado en documento público el trabajo que se viene realizando en el proyecto.  

# PENDIENTES #
1. CENIT reserva espacio y logística asociada. FARN cierra sus respectivos invitados y define posibilidades de incorporación y uso de la plataforma en diferentes ámbitos/iniciativas. Se siguen evaluando posibilidades de postulación a financiamientos. 
2. Posibilidad de hacer charla previa al 20/12.
3. JF descarga de datasets.
4. GA vuelve a contactar a Educ Ambiental de U2.
5. Solo restaría la resolución definitiva de los pdf, pero se puede ir compartiendo. 
6. Se sigue trabajo con Guía de Ed. Ambiental. 
7. CF revisa carga de reportes en formulario de novedades que vio GA antes de cargarlo. GA define su disponibilidad ese día.
