_Participantes:_ Valeria Arza (CENIT), Leticia Castro (CENIT), Guillermina Actis (CENIT), Catalina Fixman (CENIT), Jeremías Fabiano (CENIT), Marcelo Larrocca Ruiz (FARN)

# AGENDA #
1. Gestión interna del proyecto
2. Pedido de responsable de edición de ficha portal de CC Br.
3. Definiciones sobre Relocalizaciones 
4. Balances reunión fin de año
5. Tener actualizada web de CoAct: notas, actualizaciones, secciones, etc.
6. Situación de U2 y gestión de la cuenca

# RESOLUCIONES #
1. Debates internos pormenorizados de los manejos del proyecto, los cuales varios están formalmente a cargo de CENIT-UNSAM. 
2. Una organización de Brasil iba a armar una ficha de QPR y requería información. Se les dio la casilla de correos para ello.
3. FARN todavía no pudo hacer revisión interna al respecto de este tema de la plataforma. 
4. Por contexto actual, debemos optimizar y eficientizar lo que se haga desde el proyecto. Armado de capacitación sobre QPR.
5. Cambios de sitio web o reformulación del mismo.
6. Gran incertidumbre, incluso en el seno de entes relacionados.

# PENDIENTES #
1. Información que se requiera es pública y se puede bien enviar por email.
2. Enviarles correctamente la información y ver si requieren algo más. Quedó respuesta de GA en borrador para enviar o reformular.
3. ML tendrá novedades los primeros días de enero sobre cómo seguir al respecto.
4. Se comienza a planificar vía email. 22 y 23 de febrero es la reunión general de FARN. 8/1 próxima reunión de coordinación FARN-CENIT.
5. Cuando esté todo subido a la plataforma, armar una nota de blog que remita a ello.
6. Se continúa averiguando. Posibilidad de consultar a estudiante del doctorado de CENIT.
