_Participantes:_ Valeria Arza (CENIT), Leticia Castro (CENIT), Catalina Fixman (CENIT), Guillermina Actis (CENIT), Jeremías Fabiano (CENIT), Marcelo Larrocca Ruiz (FARN), Giselle Munno Dithurbide (FARN)

# AGENDA #
1. Talleres de implementación. Prox: jueves 24/8 en formato virtual.
2. Reunión de GA planificatoria con Alsina.
3. Desarrollo-datasets.
4. I Encuentro Nacional de CC – Taller de CoAct el 28-29 de agosto.
5. Reunión sobre tema en criptomonedas.
6. Noche de los museos.
7. Entrevista de Escuela sobre problemáticas CMR: posibilidad de implementar QPR. 

# RESOLUCIONES #
1. Presentación PPT con filminas y consenso con esquema planteado, resta decidir roles por sección. Posibilidad de continuidad con nuevo ciclo de talleres con Biblioteca Popular Virrey del Pino.
2. Panorama social muy álgido se suma a necesidad de buena operatividad de plataforma para lograr sostenimiento de interés. Posibilidad de realización de talleres en la Biblioteca con expertxs-académicxs en temáticas de la plataforma. A su vez, desde R34 surgió posibilidad de charla y nexo con instituto de formación docente.
3. Enviado a desarrollador el documento con pendientes y nuevos desarrollos, el presupuesto sigue demorado.
4. GA y ML estuvieron en reunión preparatoria. Necesidad de que equipos se inscriban y hagan presentes en ambos días.
5. GA y ML no pudieron asistir.
6. Definir participantes y cantidad de horas. Sábado 23 de septiembre, de 19 a 24 o 02hs.
7. Alumnos de 6to año que querían hacer una entrevista a FARN. Muy interesados en Causa Mendoza.

# PENDIENTES #
1. GM y ML en comunicación con CF para dinámica de taller virtual de mañana.
2. Necesidad de operatividad de la plataforma. Reunión en Alsina sería viernes 1/9 desde las 18:30. GA y JF están, con posible charla/expo de investigador en Aguas. 30/9 por la mañana charla en Profesorado en partido del GBA.
3. JF sigue el tema.
4. ML y GM acomodan agenda y se anotan a jornadas del 28 y 29 en MinCyT.
5. Pedir acceso a grabación de la reunión.
6. Confirmación de FARN de quiénes estarán. Confirmación del horario y dinámica.
7. FARN sigue vínculo si lo considera, y CENIT acompaña iniciativa que haya.
