_Participantes:_ Valeria Arza (CENIT), Leticia Castro (CENIT), Catalina Fixman (CENIT), Guillermina Actis (CENIT), Jeremías Fabiano (CENIT), Marcelo Larrocca Ruiz (FARN), Giselle Munno Dithurbide (FARN)

# AGENDA #
1. Talleres de implementación y conflicto en R29.
2. Charla en ISFD de cuenca media.
3. Desarrollo-datasets. Pendientes y desarrollos: Presupuesto.
4. I Encuentro Nacional de CC en MinCyT – Presentación, taller propio y lo vivido el 28 y 29 de agosto. 
5. Materiales didácticos de U2
6. Reunión sobre criptomonedas y respectiva grabación. 
7. Noche de los museos: 23 de septiembre en Lugano.
8. Entrevista de Escuela de cuenca media sobre problemáticas CMR, posibilidad de implementar QPR.
9. Novedades en Causa Mendoza.  

# RESOLUCIONES #
1. Comunicacciones intermitentes con Biblioteca Alsina, y pendiente hablar con VdP para continuidad. Está pensado hacer reunión presencial en UNSAM o FARN, con quienes están incluidos en el subsidio. En estos dos años se buscará superar dos obstáculos tenidos de siempre: Desarrollo de plataforma, y el otro definir cuál va a ser el uso y actividades, en pos que la plataforma se utilice. Que se generen datos y se pueda hacer el análisis, con actividades específicas para ello. ML pendiente contacto con Museo en la Cuenca Baja. Creación de redes sociales de QPR sujeto a estrategia de comunicación que se elija. A partir de conflicto reciente en reserva de la cuenca, debate sobre qué hacer con datos generados en QPR: posibilidad de asentar reclamos y acciones que ya realiza FARN, así como generar narrativas al respecto, plataforma puede ser una forma de hacer visible lo que se hace en el propio territorio por organizaciones.
2. Se realizó charla en instituto de formación docente, con gran asistencia, relacionando ESI con la Ed Ambiental. Existe la posibilidad de activar más cuestiones aquí, como relacionamiento con Prof de Avellaneda.
3. Recepción de presupuesto de manos del programador: más que ajustar el nro, centramiento en cerrar el tema de plazos, que se respeten esos tres meses de forma segura. Se ha unificado servidor para que actualización y mantenimiento de la página de bienvenida (landing page) sea más fácil: ya solucionado. Conviene volcar a landing las fichas, formularios sin conexión, videos, éstos últimos quizás a mapa de QPR.
4. En nuestro taller, fueron aprox 80 personas. Se trabajaron principios de la CC, establecidos por una entidad europea y se quería ver lo que surgía desde plano local por los propios actores. Encontramos que no hay mucho conocimiento de lo que se viene realizando a nivel internacional. GA ahora organizando esa información con vistas a publicarlo. 
5. Atención a estas actividades para en futuro cercano figurar como parte de los recursos sugeridos de la cuenca. En la búsqueda de actores clave para iniciar nexos, CF tiene contactos en áreas de educación.
6. Faltó hacer pedido de grabación.
7. Además de stand (necesidad de conexión web), habrá que dar una charla de 10/15 mins. Afiche, monitor y demás materiales necesarios.
8. FARN pasó los materiales, pero no se obtuvo mucha respuesta: se insiste para retomar.
9. Cuerpo colegiado presentó comentarios a U2 sobre lineamientos plan 2023-2027. Se autoriza navegación en tramo de Cuenca Baja. Se está avaluando presentación de recurso extraordinario contra dicha Resolución. Respecto del último informe trimestral de calidad de Agua, se realizará presentación desde Cuerpo Colegiado a partir del análisis de esos informes. Desde FARN se entablaron los lineamientos del plan de emergencia de saneamiento de la cuenca. Explicación de sus alcances e implicancias.y la manera en que se elaboran planes y comunicados, y utilidad posible de QPR para ello.

# PENDIENTES #
1. ML se reuniría con representante de Museo. Existe área de investigación en FARN (volcada hoy a hidrocarburos y transición energética), que en un futuro podría activarse para CoAct. FARN: subir estos reportes como novedades en QPR.
2. CENIT dialoga con Co90 para trabajo de procesamiento para de los talleres de VdP.
3. ML comparte presupuesto a CENIT, que da respuesta del mismo esta semana. VA, CF y JF se reúnen para avanzar con tareas asociadas.
4. GA comparte a ML los dos insumos: 10 principios de CC y Características de la CC.
5. Pedir reunión a P2 como actor clave para nexo con esta área.
6. GA pide la grabación de la reunión.
7. GA averigua por un monitor. JF se encarga de afiche y banners.
8. ML retoma diálogo con esa escuela.
9. FARN incorpora datos de QPR como insumo para acciones propias en la causa.
