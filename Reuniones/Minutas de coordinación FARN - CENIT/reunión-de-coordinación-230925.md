_Participantes:_ Valeria Arza (CENIT), Leticia Castro (CENIT), Guillermina Actis (CENIT), Jeremías Fabiano (CENIT), Marcelo Larrocca Ruiz (FARN).

# AGENDA #
1. Talleres de implementación  
2. Reunión/contacto de ML con representante de museo histórico en cuenca baja.
3. Desarrollo y datasets. 
4. Materiales didácticos de U2 y posibilidad de reunión con P2
5. Entrevista con colegio de la cuenca sobre problemáticas CMR: ¿diálogos? 

# RESOLUCIONES #
1. Se tuvo reunión planificatoria con Ci7 para planear taller del siguiente viernes en Biblioteca de Alsina, además de talleres asociados por venir. Con Virrey del Pino restan definir próximas instancias a plantearse. Además, JF y CF irán a dar taller a ISFD de cuenca media el 12 de octubre por la tarde. Por último, llegó contacto de docente universitario sobre realización de muestra en un centro cult del centro porteño el 23 de noviembre.
2. No se tuvo noticias y ML insistirá en el trancurso de la semana.
3. Charlas sobre pendientes específicos de desarrollo.
4. Como estrategia del nuevo proyecto, necesidad de tener un set de datos que se puedan actualizar: insertar la plataforma como un insumo clave en educación ambiental. Más allá de experiencias realizadas, necesidad de contactar personas especialistas en educación por carecer de ello en el propio equipo. Cuestión de financiamiento como arista a considerarse, y realizar búsqueda de las opciones sobre todo para destinar a recursos humanos en pos de facilitar la sostenibilidad. En otro sentido posibilidad de navegación en el Riachuelo. Por último, posibilidad de hacer capacitación interareal de QPR en FARN.
5. El vínculo lo realizó finalmente ML: está establecido y se le pasaron materiales informativos de CoAct. 

# PENDIENTES #
1. GA: pide más detalles sobre actividad de extensión en centro cultural. JF y CF: realizan charla en Prof. de La Matanza el 12 de octubre. JF: contacta aliades para reescritura de guía didáctica. 
2. ML sigue el tema 
3. JF lista TDR. VA y ML mueven contrato del desarrollador.
4. LC: contacta a proyecto de turismo comunitario. ML y GM: buscan cómo transmitir QPR otras áreas de la organización. VA: manda mensaje a director de FARN para movilizar. 
5. ML: vuelve a contactar
