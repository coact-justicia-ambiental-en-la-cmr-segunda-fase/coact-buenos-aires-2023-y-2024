_Participantes:_ Valeria Arza (CENIT), Leticia Castro (CENIT), , Guillermina Actis (CENIT), Jeremías Fabiano (CENIT), Marcelo Larrocca Ruiz (FARN)

# AGENDA #

1. Talleres de implementación. Prox: miércoles 9/8 en R34 (Pulmón Verde)
2. Datasets: desarrollos deseables y reciente reunión con empresa de desarrolladores 
3. Programa Nacional de Ciencia Ciudadana – Presentación el 28-29 de agosto 
4. Consulta espontánea e invitación a reunión de criptomonedas para causas ambientales
5. Noche de los museos con Junta de Estudios históricos y culturales de Villa Lugano y Villa Riachuelo
6. Decidir cuándo tener próx reunión (21/8 es feriado)

# RESOLUCIONES #

1. Se decidió sobre materiales a llevar, participantes y rol de cada une
2. Temas a arreglar de la plataforma: entrada al dashboard, recuperación de usuarios, formualario de novedades y más. Proz y cons de implementación de tecnología terecerizada. Por lo pronto, elaboración de listado en archivo con pendientes y deseables tecnológicos a aplicar.
3. Será en el Polo Tecnológico, el lunes 28 de agosto al mediodía. Calcúlale 11:30 y las 12 y duración de una hora y media. Se esperan aprox 100 personas.
4. Es informativa, y puede servirnos para ver si hay alguien desarrollando
5. 23 de septiembre y duración de toda la noche. Posibilidad de relacionar la plataforma con los 15 años de la causa y difundir la guía.
6. 23/8 a las 14:00hs

# PENDIENTES #

1. ML, GM, CF, JF y persona de organización de aves van a taller en auto de ML
2. Se integran pendientes al documento con desarrollador.
JF: Armado lista de deseables para que se tenga presupuesto.
3. Necesidad de participación de todes para asistir.
4. ML pide más información y le manda a GA.
. GA quizás asiste
5. Sábado 23/09 en Junta de Lugano.
. CF define su posibilidad de asistencia.
. Definimos si charla o stand.
6. ML confirma su disponibilidad para el 23/8 a las 14hs
